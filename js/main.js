var EL_REFERERS = $('textarea[name=referers]'),
    EL_PAGES = $('textarea[name=pages]'),
    EL_DEPTH = $('textarea[name=depth]'),
    EL_RESULT = $('textarea[name=result]')

var generator = new (extSource)

function extSource() {
    es = {}
    es["Items"] = [{}]
    this.setReferers = function (aReferers) {
        if (aReferers) {
            es["Items"][0]["Referers"] = []
            var referers = aReferers.split("\n")
            for (var i = 0; i < referers.length; i++) {
                var referer = {}
                referer["Referer"] = referers[i].replace("\r","")
                es["Items"][0]["Referers"].push(referer)
            }
        }
    }
    this.setPages = function (aPages) {
        if (aPages) {
            es["Items"][0]["Pages"] = []
            var pages = aPages.split("\n")
            for (var i = 0; i < pages.length; i++) {
                var page = {}
                page["Page"] = pages[i].replace("\r","");
                es["Items"][0]["Pages"].push(page)
            }
        }
    }
    this.setDepths = function (aDepths) {
        if (aDepths) {
            es["Items"][0]["Paths"] = []
            var depths = aDepths.split("\n")
            for (var i = 0; i < depths.length; i++) {
                var depth=parseInt(depths[i]);
                var path={}
                path["Path"]=[]
                for (var j = 0; j < depth; j++) {
                    path["Path"].push("/");
                }
                es["Items"][0]["Paths"].push(path)
            }
        }
    }
    this.result = function () {
        return JSON.stringify(es)
    }
}

$('.btn-gen').on('click', function () {
    generator.setReferers(EL_REFERERS.val())
    generator.setPages(EL_PAGES.val())
    generator.setDepths(EL_DEPTH.val())
    EL_RESULT.val(generator.result())
})

var formSet = new TFormSet()

function TFormSet() {
    this.completedForm = function () {
        EL_REFERERS.val(localStorage.getItem('gen-referers'))
        EL_PAGES.val(localStorage.getItem('gen-pages'))
        EL_DEPTH.val(localStorage.getItem('gen-depth'))
    }
    this.saveForm = function () {
        localStorage.setItem('gen-referers', EL_REFERERS.val())
        localStorage.setItem('gen-pages', EL_PAGES.val())
        localStorage.setItem('gen-depth', EL_DEPTH.val())
    }
    this.resetForm = function () {
        EL_REFERERS.val('')
        EL_PAGES.val('')
        EL_DEPTH.val('')
        localStorage.removeItem('gen-referers')
        localStorage.removeItem('gen-pages')
        localStorage.removeItem('gen-depth')
    }
}

$(document).ready(function () {
    formSet.completedForm()
})

$('.btn-clear').on('click', formSet.resetForm)

EL_REFERERS.on('keyup', formSet.saveForm)
EL_PAGES.on('keyup', formSet.saveForm)
EL_DEPTH.on('keyup', formSet.saveForm)